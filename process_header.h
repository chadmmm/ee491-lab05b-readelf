#ifndef PROCESS_HEADER_H
#define PROCESS_HEADER_H
///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05b - Read ELF
/////
///// @file process_header.h
///// @version 1.0
/////
///// @author Chad Morita <chadmmm@hawaii.edu>
///// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
///// @date   21_02_2021
/////////////////////////////////////////////////////////////////////////////////
#include "elf.h"

void process_header_32(char * filename, Elf32_Ehdr *, FILE**);
void process_header_64(char * filename, Elf64_Ehdr *, FILE**);
int initialize_state(char * filename);

#endif
