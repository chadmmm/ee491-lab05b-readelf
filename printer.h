#ifndef PRINT_HEADER_H
#define PRINT_HEADER_H
///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05b - Read ELF
/////
///// @file printer.h
///// @version 1.0
/////
///// @author Chad Morita <chadmmm@hawaii.edu>
///// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
///// @date   21_02_2021
/////////////////////////////////////////////////////////////////////////////////

#include "elf.h"

int process_header(char * );
int print_header_32(Elf32_Ehdr *);
int print_header_64(Elf64_Ehdr *);
void print_sections_32(Elf32_Ehdr, void *, void *);
void print_sections_64(Elf64_Ehdr, void *, void *);
void hex_dump_32(Elf32_Ehdr, void *, void *, char *);
void hex_dump_64(Elf64_Ehdr, void *, void *, char *);

#endif
