#ifndef UTIL_H
#define UTIL_H
///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05b - Read ELF
/////
///// @file util.h
///// @version 2.0
/////
///// @author Chad Morita <chadmmm@hawaii.edu>
///// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
///// @date   21_02_2021
/////////////////////////////////////////////////////////////////////////////////
#include <stdint.h>
#include <stdbool.h>

uint16_t swapUint16(uint16_t swapthis);
uint32_t swapUint32(uint32_t swapthis);
uint64_t swapUint64(uint64_t swapthis);
bool isSameEndianness();
bool isNum(char *);
bool isAscii(char);

#endif
