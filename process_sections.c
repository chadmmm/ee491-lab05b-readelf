///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Read ELF
///
/// @file process_sections.c
/// @version 2.0
///
/// @author Chad Morita <chadmmm@hawaii.edu>
/// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
/// @date   21_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "readelf.h"
#include "elf.h"
#include "util.h"

void process_sections_32(char * filename, Elf32_Ehdr header, void ** file_pointer, size_t file_size, void ** string_table_pointer) {
   struct stat file_status;
   Elf32_Shdr * section_header;

   int file = open(filename, O_RDONLY);
  
   // Try to open the file
   if ( file == -1 ) {
      fprintf(stderr, "readelf: Error: '%s' no such file\n", filename);
      exit(EXIT_FAILURE);
   }

   // Try to stat the file
   if ( fstat(file, &file_status)  != 0) {
      fprintf(stderr, "readelf: %s: Error: Unable to get file size\n", filename);
      exit(EXIT_FAILURE);
   }

   // Map the file and check for errors
   *file_pointer = mmap(NULL, file_status.st_size, PROT_READ, MAP_PRIVATE, file, 0);
   if ( *file_pointer == MAP_FAILED ) {
      fprintf(stderr, "readelf: Error: '%s' unable to map file\n", filename);
      exit(EXIT_FAILURE);
   }


   // check for endianness mismatch
   if (isSameEndianness()) {

      // point to the beginning of the section header section
      section_header = *file_pointer + header.e_shoff;
   
      // loop through all sections to get the section header string table index
      for (int i=0; i<header.e_shnum; i++) {
         if (i == header.e_shstrndx ) {
            *string_table_pointer = section_header->sh_offset + *file_pointer;
         }
         section_header++;
      }

   } else { // mismatched endianness
      
      // point to the beginning of the section header section
      section_header = *file_pointer + swapUint64(header.e_shoff);
   
      // loop through all sections to get the section header string table index
      for (int i=0; i<swapUint16(header.e_shnum); i++) {
         if (i == swapUint16(header.e_shstrndx) ) {
            *string_table_pointer = swapUint64(section_header->sh_offset) + *file_pointer;
         }
         section_header++;
      }
   }
}


/*
 * @brief maps the elf file and locates the string table offset
 * @param file char * filename ELF file to load
 * @param Elf64_Ehdr header struct containing ELF header, needs to be read in prior using process_header_64
 * @param void ** file_ponter pointer to beginning of mapped file
 * @param void ** string_table_pointer pointer to the mapped section header string table
 * @return void
 */
void process_sections_64(char * filename, Elf64_Ehdr header, void ** file_pointer, size_t file_size, void ** string_table_pointer) {
   struct stat file_status;
   Elf64_Shdr * section_header;

   int file = open(filename, O_RDONLY);
  
   // Try to open the file
   if ( file == -1 ) {
      fprintf(stderr, "readlef: Error: '%s' no such file\n", filename);
      exit(EXIT_FAILURE);
   }

   // Try to stat the file
   if ( fstat(file, &file_status)  != 0) {
      fprintf(stderr, "readelf: %s: Error: Unable to get file size\n", filename);
      exit(EXIT_FAILURE);
   }

   // Map the file and check for errors
   *file_pointer = mmap(NULL, file_status.st_size, PROT_READ, MAP_PRIVATE, file, 0);
   if ( *file_pointer == MAP_FAILED ) {
      fprintf(stderr, "readelf: Error: '%s' unable to map file\n", filename);
      exit(EXIT_FAILURE);
   }


   // check for endianness mismatch
   if (isSameEndianness()) {

      // point to the beginning of the section header section
      section_header = *file_pointer + header.e_shoff;
   
      // loop through all sections to get the section header string table index
      for (int i=0; i<header.e_shnum; i++) {
         if (i == header.e_shstrndx ) {
            *string_table_pointer = section_header->sh_offset + *file_pointer;
         }
         section_header++;
      }

   } else { // mismatched endianness
      
      // point to the beginning of the section header section
      section_header = *file_pointer + swapUint64(header.e_shoff);
   
      // loop through all sections to get the section header string table index
      for (int i=0; i<swapUint16(header.e_shnum); i++) {
         if (i == swapUint16(header.e_shstrndx) ) {
            *string_table_pointer = swapUint64(section_header->sh_offset) + *file_pointer;
         }
         section_header++;
      }
   }

}
 
