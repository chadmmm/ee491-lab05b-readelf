###############################################################################
### University of Hawaii, College of Engineering
### EE 491 - Software Reverse Engineering
### Lab 05b - Read ELF
###
### @file Makefile.c
### @version 1.0
###
### @author Chad Morita <chadmmm@hawaii.edu>
### @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
### @date   21_02_2021
###############################################################################

# Build a Read ELF program

CC     = gcc
CFLAGS = -c -g -Wall

TARGET = readelf

all: $(TARGET)

$(TARGET): $(TARGET).o process_header.o printer.o util.o process_sections.o
	$(CC) -o $(TARGET) $(TARGET).o process_header.o printer.o util.o process_sections.o

$(TARGET).o: $(TARGET).c
	$(CC) $(CFLAGS) $(TARGET).c

process_header.o: process_header.c
	$(CC) $(CFLAGS) process_header.c

printer.o: printer.c
	$(CC) $(CFLAGS) printer.c

util.o: util.c
	$(CC) $(CFLAGS) util.c

process_sections.o: process_sections.c
	$(CC) $(CFLAGS) process_sections.c

test: $(TARGET)
	./$(TARGET) -h $(TARGET)

clean:
	rm $(TARGET) *.o


