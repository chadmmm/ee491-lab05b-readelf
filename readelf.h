#ifndef READELF_H
#define READELF_H
///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05b - Read ELF
/////
///// @file readelf.h
///// @version 1.0
/////
///// @author Chad Morita <chadmmm@hawaii.edu>
///// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
///// @date   21_02_2021
/////////////////////////////////////////////////////////////////////////////////

void usage();

enum Endianness{big_endian, little_endian};

typedef struct {
   enum Endianness elf_endianness;
   enum Endianness  machine_endianness;
   int elf_architecture;
} ProgramState;

extern ProgramState program_state;


#endif
