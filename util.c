///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Read ELF
///
/// @file util.c.c
/// @version 2.0
///
/// @author Chad Morita <chadmmm@hawaii.edu>
/// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
/// @date   21_02_2021
///////////////////////////////////////////////////////////////////////////////

#include "util.h"
#include "readelf.h"

/*
 * @brief swap endianness for uint16_t
 * @param uint16_t swapthis data to be swapped
 * @return swapped endian data
 */
uint16_t swapUint16(uint16_t swapthis) {
   union {
      uint16_t machineWord;
      unsigned char bytes[2];
   } overlay;

   overlay.machineWord = swapthis;
   
   unsigned char temp;
   temp             = overlay.bytes[0];
   overlay.bytes[0] = overlay.bytes[1];
   overlay.bytes[1] = temp;

   return overlay.machineWord;
}

/*
 * @brief swap endianness for uint32_t
 * @param uint32_t swapthis data to be swapped
 * @return swapped endian data
 */
uint32_t swapUint32(uint32_t swapthis) {
   union {
      uint32_t machineWord;
      unsigned char bytes[4];
   } overlay;

   overlay.machineWord = swapthis;
   
   unsigned char temp;
   temp             = overlay.bytes[0];
   overlay.bytes[0] = overlay.bytes[3];
   overlay.bytes[3] = temp;
   
   temp             = overlay.bytes[1];
   overlay.bytes[1] = overlay.bytes[2];
   overlay.bytes[2] = temp;

   return overlay.machineWord;
}

/*
 * @brief swap endianness for uint32_t
 * @param uint32_t swapthis data to be swapped
 * @return swapped endian data
 */
uint64_t swapUint64(uint64_t swapthis) {
   union {
      uint64_t machineWord;
      unsigned char bytes[8];
   } overlay;

   overlay.machineWord = swapthis;
   
   unsigned char temp;
   temp             = overlay.bytes[0];
   overlay.bytes[0] = overlay.bytes[7];
   overlay.bytes[7] = temp;
   
   temp             = overlay.bytes[1];
   overlay.bytes[1] = overlay.bytes[6];
   overlay.bytes[6] = temp;

   temp             = overlay.bytes[2];
   overlay.bytes[2] = overlay.bytes[5];
   overlay.bytes[5] = temp;
   
   temp             = overlay.bytes[3];
   overlay.bytes[3] = overlay.bytes[4];
   overlay.bytes[4] = temp;

   return overlay.machineWord;
}


/*
 * @brief determines whether or not the ELF endianness matches the machine endianness
 * @return true if match false if mismatch
 */
bool isSameEndianness() {
   return program_state.elf_endianness == program_state.machine_endianness;
}

/*
 * @brief determines whether or not an ASCII string is a number represented in ASCII
 * @return true if an ASCII number false otherwise
 */
bool isNum(char * str) {
   int i = 0;

   // Iterate through each character of str until you encounter a null terminator
   while(str[i] != '\0') {
      // If a character is not in the range of ASCII numbers
      if ( (str[i] < 0x30) || (str[i] > 0x39) ) {
         return false;
      }

      i++;
   }

   // else return true
   return true;
}

/*
 * @brief determines whether or not a character is an ASCII character
 * @return true if an ASCII false otherwise
 */
bool isAscii(char c) {
   return (c > 0x20) && (c < 0x7f);
}
