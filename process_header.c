///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Read ELF
///
/// @file readelf.c
/// @version 1.0
///
/// @author Chad Morita <chadmmm@hawaii.edu>
/// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
/// @date   21_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "printer.h"
#include "readelf.h"


/*
 * #brief Read in header information from the ELF file for a
 *       32 bit header
 * @param char * filename filename of the ELF file
 * @param Elf32_Ehdr * pointer to the address of the header struct
 *
 */
void process_header_32(char * filename, Elf32_Ehdr * header, FILE ** fp) {

   // Try to open the file
   if ( (*fp = fopen(filename, "r")) == NULL) {
      fprintf(stderr, "readelf: Error: '%s': No such file\n", filename);
      exit(EXIT_FAILURE);
   }

   // read in the header starting from the beginning again
   pread(fileno(*fp), header, sizeof(Elf32_Ehdr), 0);

}

/*
 * #brief Read in header information from the ELF file for a
 *       32 bit header
 * @param char * filename filename of the ELF file
 * @param Elf64_Ehdr * pointer to the address of the header struct
 *
 */
void process_header_64(char * filename, Elf32_Ehdr * header, FILE ** fp) {

   // Try to open the file
   if ( (*fp = fopen(filename, "r")) == NULL) {
      fprintf(stderr, "readelf: Error: '%s': No such file\n", filename);
      exit(EXIT_FAILURE);
   }

   // read in the header starting from the beginning again
   pread(fileno(*fp), header, sizeof(Elf64_Ehdr), 0);

}


/*
 * @breif Reads in the e_ident section of the ELF file and initializes the
 *       global struct with machine state
 * @param char * filename filename of teh ELF file
 * @return void
 */
void initialize_state(char * filename) {

   FILE * fp;
   unsigned char e_ident[16];
   int n = 1; // Used to check machine endianness

   if ( (fp = fopen(filename, "r")) == NULL ) {
      fprintf(stderr, "readelf: Error: '%s': No such file\n", filename);
      exit(EXIT_FAILURE);
   }

   pread(fileno(fp), e_ident, 16, 0);

   // Check ELF file architecture
   if ( e_ident[4] == ELFCLASS32 ) { // 32 bit ELF file
      program_state.elf_architecture = ELFCLASS32;
   } else if ( e_ident[4] == ELFCLASS64 ) { // 64 bit ELF file
      program_state.elf_architecture = ELFCLASS64;
   } else { // Unable to determine architecture
      exit(EXIT_FAILURE); 
   }

   // Check ELF file endianness
   if ( e_ident[5] == ELFDATA2LSB ) { // ELF file is little endina
      program_state.elf_endianness = little_endian;
   } else if ( e_ident[5] == ELFDATA2MSB ) { // ELF file is big endian
      program_state.elf_endianness = big_endian;
   } else { // Unable to determine ELF file endianness
      exit(EXIT_FAILURE);
   }

   // Check machine endianness
   if ( *(char *)&n == 1) { // little endian if true
      program_state.machine_endianness = little_endian;
   } else {
      program_state.machine_endianness = big_endian;
   }

}
