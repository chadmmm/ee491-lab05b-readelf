///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Read ELF
///
/// @file readelf.c
/// @version 1.0
///
/// @author Chad Morita <chadmmm@hawaii.edu>
/// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
/// @date   21_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <elf.h>
#include <arpa/inet.h>
#include <string.h>
#include "readelf.h"
#include "util.h"


/*
 * @brief Print header information from the ELF file
 * @param ELF32_Ehdr * header pointer to the 32 bit header
 * @return 0 success, 1 failure
 *
 */
int print_header_32 (Elf32_Ehdr * header) {

   // Print beginning of header output
   printf("ELF Header:\n");
   printf("  Magic:   ");

   // 32 bit, little endian
   if ( (header->e_ident[4] == ELFCLASS32) && (header->e_ident[5] == ELFDATA2LSB) ) {

      // Print the Magic section
      for(int i=0; i<16; i++) {
         printf("%02x ", header->e_ident[i]);
      }
      printf("\n"); // newline at the end

      printf("  Class:                             ELF64\n"); // No need to recheck
      printf("  Data:                              2's complement, little endian\n"); // No need to recheck

      // Version
      if (header->e_ident[6] == EV_CURRENT) {
         printf("  Version:                           1 (current)\n");
      }

      // OSABI
      printf("  OS/ABI:                            ");
      switch(header->e_ident[7]) {
         case ELFOSABI_SYSV:
            printf("UNIX - System V\n");
            break;

         case ELFOSABI_HPUX:
            printf("HP - UX\n");
            break;
        
         case ELFOSABI_NETBSD:
            printf("NetBSD\n");
            break;
        
         case ELFOSABI_LINUX:
            printf("Linux\n");
            break;
        
         case ELFOSABI_SOLARIS:
            printf("Solaris\n");
            break;
        
         case ELFOSABI_IRIX:
            printf("IRIX\n");
            break;
         
         case ELFOSABI_FREEBSD:
            printf("FreeBSD\n");
            break;
        
         case ELFOSABI_TRU64:
            printf("TRU64 UNIX\n");
            break;
        
         case ELFOSABI_ARM:
            printf("ARM architecture\n");
            break;
        
         case ELFOSABI_STANDALONE:
            printf("Stand-alone (embedded)\n");
            break;
      }

      // ABI Version
      printf("  ABI Version:                       %d\n", header->e_ident[8]); 

      // Type
      printf("  Type:                              ");
      switch(header->e_type) {
         case ET_REL:
            printf("REL (Relocatable file)\n");
            break;
         
         case ET_EXEC:
            printf("EXEC (Executable file)\n");
            break;
         
         case ET_DYN:
            printf("DYN (Shared object file)\n");
            break;

         case ET_CORE:
            printf("CORE (Core file)\n");
            break;

      }

      // Machine
      printf("  Machine:                           ");
      switch(header->e_machine) {
         case EM_M32:
            printf("AT&T WE 32100\n");
            break;

         case EM_SPARC:
            printf("Sun Microsystems SPARC\n");
            break;
         
         case EM_386:
            printf("Intel 80386\n");
            break;

         case EM_68K:
            printf("Motorola 68000\n");
            break;
         
         case EM_88K:
            printf("Motorola 88000\n");
            break;

         case EM_860:
            printf("Intel 80860\n");
            break;

         case EM_MIPS:
            printf("MIPS RS3000 (big-endian only\n");
            break;

         case EM_PARISC:
            printf("HP/PA\n");
            break;
         
         case EM_SPARC32PLUS:
            printf("SPARC with enhanced instruction set\n");
            break;

         case EM_PPC:
            printf("PowerPC\n");
            break;
         
         case EM_PPC64:
            printf("PowerPC64\n");
            break;
         
         case EM_S390:
            printf("IBM S/390\n");
            break;
         
         case EM_ARM:
            printf("Advanced RISC Machines\n");
            break;
         
         case EM_SH:
            printf("Renesas SuperH\n");
            break;
         
         case EM_SPARCV9:
            printf("SPARC V9 64-bit\n");
            break;

         case EM_IA_64:
            printf("Intel Itanium\n");
            break;
         
         case EM_X86_64:
            printf("Advanced Micro Devices X86-64\n");
            break;
         
         case EM_VAX:
            printf("DEC Vax\n");
            break;
         
         case EM_AARCH64:
            printf("AArch64\n");
            break;
      }

      // Version
      if (header->e_version == EV_CURRENT) {
         printf("  Version:                           0x1\n");
      }

      // Entry point address
      printf("  Entry point address:               %x\n", header->e_entry);

      // Start of program headers
      printf("  Start of program headers:          %d (bytes into file)\n", header->e_phoff);
     
      // Start of section headers
      printf("  Start of section headers:          %d (bytes into file)\n", header->e_shoff);
     
      // Flags
      printf("  Flags:                             0x0\n");

      // Size of this header
      printf("  Size of this header:               %d (bytes)\n", header->e_ehsize);

      // Size of program headers
      printf("  Size of program headers:           %d (bytes)\n", header->e_phentsize);
      
      // Number of program headers
      printf("  Number of program headers:         %d\n", header->e_phnum);

      // Size of section headers
      printf("  Size of section headers:           %d (bytes)\n", header->e_shentsize);

      // Number of section headers
      printf("  Number of section headers:         %d\n", header->e_shnum);
      
      // Section header string table index
      printf("  Section header string table index: %d\n", header->e_shstrndx);

   } 
   
   // 32 bit, big endian
   if ( (header->e_ident[4] == ELFCLASS32) && (header->e_ident[5] == ELFDATA2MSB) ) {

      // Print the Magic section
      for(int i=0; i<16; i++) {
         printf("%02x ", header->e_ident[i]);
      }
      printf("\n"); // newline at the end

      printf("  Class:                             ELF64\n"); // No need to recheck
      printf("  Data:                              2's complement, little endian\n"); // No need to recheck

      // Version
      if (header->e_ident[6] == EV_CURRENT) {
         printf("  Version:                           1 (current)\n");
      }

      // OSABI
      printf("  OS/ABI:                            ");
      switch(header->e_ident[7]) {
         case ELFOSABI_SYSV:
            printf("UNIX - System V\n");
            break;

         case ELFOSABI_HPUX:
            printf("HP - UX\n");
            break;
        
         case ELFOSABI_NETBSD:
            printf("NetBSD\n");
            break;
        
         case ELFOSABI_LINUX:
            printf("Linux\n");
            break;
        
         case ELFOSABI_SOLARIS:
            printf("Solaris\n");
            break;
        
         case ELFOSABI_IRIX:
            printf("IRIX\n");
            break;
         
         case ELFOSABI_FREEBSD:
            printf("FreeBSD\n");
            break;
        
         case ELFOSABI_TRU64:
            printf("TRU64 UNIX\n");
            break;
        
         case ELFOSABI_ARM:
            printf("ARM architecture\n");
            break;
        
         case ELFOSABI_STANDALONE:
            printf("Stand-alone (embedded)\n");
            break;
      }

      // ABI Version
      printf("  ABI Version:                       %d\n", header->e_ident[8]); 

      // Type
      printf("  Type:                              ");
      switch(ntohs(header->e_type)) {
         case ET_REL:
            printf("REL (Relocatable file)\n");
            break;
         
         case ET_EXEC:
            printf("EXEC (Executable file)\n");
            break;
         
         case ET_DYN:
            printf("DYN (Shared object file)\n");
            break;

         case ET_CORE:
            printf("CORE (Core file)\n");
            break;

      }

      // Machine
      printf("  Machine:                           ");
      switch(ntohs(header->e_machine)) {
         case EM_M32:
            printf("AT&T WE 32100\n");
            break;

         case EM_SPARC:
            printf("Sun Microsystems SPARC\n");
            break;
         
         case EM_386:
            printf("Intel 80386\n");
            break;

         case EM_68K:
            printf("Motorola 68000\n");
            break;
         
         case EM_88K:
            printf("Motorola 88000\n");
            break;

         case EM_860:
            printf("Intel 80860\n");
            break;

         case EM_MIPS:
            printf("MIPS RS3000 (big-endian only\n");
            break;

         case EM_PARISC:
            printf("HP/PA\n");
            break;
         
         case EM_SPARC32PLUS:
            printf("SPARC with enhanced instruction set\n");
            break;

         case EM_PPC:
            printf("PowerPC\n");
            break;
         
         case EM_PPC64:
            printf("PowerPC64\n");
            break;
         
         case EM_S390:
            printf("IBM S/390\n");
            break;
         
         case EM_ARM:
            printf("Advanced RISC Machines\n");
            break;
         
         case EM_SH:
            printf("Renesas SuperH\n");
            break;
         
         case EM_SPARCV9:
            printf("SPARC V9 64-bit\n");
            break;

         case EM_IA_64:
            printf("Intel Itanium\n");
            break;
         
         case EM_X86_64:
            printf("Advanced Micro Devices X86-64\n");
            break;
         
         case EM_VAX:
            printf("DEC Vax\n");
            break;

			case EM_AARCH64:
            printf("AArch64\n");
            break;
      }

      // Version
      if (ntohl(header->e_version) == EV_CURRENT) {
         printf("  Version:                           0x1\n");
      }

      // Entry point address
      printf("  Entry point address:               %x\n", header->e_entry);

      // Start of program headers
      printf("  Start of program headers:          %d (bytes into file)\n", header->e_phoff);
     
      // Start of section headers
      printf("  Start of section headers:          %d (bytes into file)\n", header->e_shoff);
     
      // Flags
      printf("  Flags:                             0x0\n");

      // Size of this header
      printf("  Size of this header:               %d (bytes)\n", header->e_ehsize);

      // Size of program headers
      printf("  Size of program headers:           %d (bytes)\n", header->e_phentsize);
      
      // Number of program headers
      printf("  Number of program headers:         %d\n", header->e_phnum);

      // Size of section headers
      printf("  Size of section headers:           %d (bytes)\n", header->e_shentsize);

      // Number of section headers
      printf("  Number of section headers:         %d\n", header->e_shnum);
      
      // Section header string table index
      printf("  Section header string table index: %d\n", header->e_shstrndx);
   } 


   return 0;

}


/*
 * @brief Print header information from the ELF file
 * @param ELF64 * header pointer to the 64 bit header
 * @return 0 success, 1 failure
 *
 */
int print_header_64 (Elf64_Ehdr * header) {

   // Print beginning of header output
   printf("ELF Header:\n");
   printf("  Magic:   ");

   // 64 bit, little endian
   if ( (header->e_ident[4] == ELFCLASS64) && (header->e_ident[5] == ELFDATA2LSB) ) {

      // Print the Magic section
      for(int i=0; i<16; i++) {
         printf("%02x ", header->e_ident[i]);
      }
      printf("\n"); // newline at the end

      printf("  Class:                             ELF64\n"); // No need to recheck
      printf("  Data:                              2's complement, little endian\n"); // No need to recheck

      // Version
      if (header->e_ident[6] == EV_CURRENT) {
         printf("  Version:                           1 (current)\n");
      }

      // OSABI
      printf("  OS/ABI:                            ");
      switch(header->e_ident[7]) {
         case ELFOSABI_SYSV:
            printf("UNIX - System V\n");
            break;

         case ELFOSABI_HPUX:
            printf("HP - UX\n");
            break;
        
         case ELFOSABI_NETBSD:
            printf("NetBSD\n");
            break;
        
         case ELFOSABI_LINUX:
            printf("Linux\n");
            break;
        
         case ELFOSABI_SOLARIS:
            printf("Solaris\n");
            break;
        
         case ELFOSABI_IRIX:
            printf("IRIX\n");
            break;
         
         case ELFOSABI_FREEBSD:
            printf("FreeBSD\n");
            break;
        
         case ELFOSABI_TRU64:
            printf("TRU64 UNIX\n");
            break;
        
         case ELFOSABI_ARM:
            printf("ARM architecture\n");
            break;
        
         case ELFOSABI_STANDALONE:
            printf("Stand-alone (embedded)\n");
            break;
      }

      // ABI Version
      printf("  ABI Version:                       %d\n", header->e_ident[8]); 

      // Type
      printf("  Type:                              ");
      switch(header->e_type) {
         case ET_REL:
            printf("REL (Relocatable file)\n");
            break;
         
         case ET_EXEC:
            printf("EXEC (Executable file)\n");
            break;
         
         case ET_DYN:
            printf("DYN (Shared object file)\n");
            break;

         case ET_CORE:
            printf("CORE (Core file)\n");
            break;

      }

      // Machine
      printf("  Machine:                           ");
      switch(header->e_machine) {
         case EM_M32:
            printf("AT&T WE 32100\n");
            break;

         case EM_SPARC:
            printf("Sun Microsystems SPARC\n");
            break;
         
         case EM_386:
            printf("Intel 80386\n");
            break;

         case EM_68K:
            printf("Motorola 68000\n");
            break;
         
         case EM_88K:
            printf("Motorola 88000\n");
            break;

         case EM_860:
            printf("Intel 80860\n");
            break;

         case EM_MIPS:
            printf("MIPS RS3000 (big-endian only\n");
            break;

         case EM_PARISC:
            printf("HP/PA\n");
            break;
         
         case EM_SPARC32PLUS:
            printf("SPARC with enhanced instruction set\n");
            break;

         case EM_PPC:
            printf("PowerPC\n");
            break;
         
         case EM_PPC64:
            printf("PowerPC64\n");
            break;
         
         case EM_S390:
            printf("IBM S/390\n");
            break;
         
         case EM_ARM:
            printf("Advanced RISC Machines\n");
            break;
         
         case EM_SH:
            printf("Renesas SuperH\n");
            break;
         
         case EM_SPARCV9:
            printf("SPARC V9 64-bit\n");
            break;

         case EM_IA_64:
            printf("Intel Itanium\n");
            break;
         
         case EM_X86_64:
            printf("Advanced Micro Devices X86-64\n");
            break;
         
         case EM_VAX:
            printf("DEC Vax\n");
            break;

			case EM_AARCH64:
            printf("AArch64\n");
            break;
      }

      // Version
      if (header->e_version == EV_CURRENT) {
         printf("  Version:                           0x1\n");
      }

      // Entry point address
      printf("  Entry point address:               %p\n", (void *) header->e_entry);

      // Start of program headers
      printf("  Start of program headers:          %ld (bytes into file)\n", header->e_phoff);
     
      // Start of section headers
      printf("  Start of section headers:          %ld (bytes into file)\n", header->e_shoff);
     
      // Flags
      printf("  Flags:                             0x0\n");

      // Size of this header
      printf("  Size of this header:               %d (bytes)\n", header->e_ehsize);

      // Size of program headers
      printf("  Size of program headers:           %d (bytes)\n", header->e_phentsize);
      
      // Number of program headers
      printf("  Number of program headers:         %d\n", header->e_phnum);

      // Size of section headers
      printf("  Size of section headers:           %d (bytes)\n", header->e_shentsize);

      // Number of section headers
      printf("  Number of section headers:         %d\n", header->e_shnum);
      
      // Section header string table index
      printf("  Section header string table index: %d\n", header->e_shstrndx);


   }
  
   // 64 bit, big endian
   if ( (header->e_ident[4] == ELFCLASS64) && (header->e_ident[5] == ELFDATA2MSB) ) {

      // Print the Magic section
      for(int i=0; i<16; i++) {
         printf("%02x ", header->e_ident[i]);
      }
      printf("\n"); // newline at the end

      printf("  Class:                             ELF64\n"); // No need to recheck
      printf("  Data:                              2's complement, little endian\n"); // No need to recheck

      // Version
      if (header->e_ident[6] == EV_CURRENT) {
         printf("  Version:                           1 (current)\n");
      }

      // OSABI
      printf("  OS/ABI:                            ");
      switch(header->e_ident[7]) {
         case ELFOSABI_SYSV:
            printf("UNIX - System V\n");
            break;

         case ELFOSABI_HPUX:
            printf("HP - UX\n");
            break;
        
         case ELFOSABI_NETBSD:
            printf("NetBSD\n");
            break;
        
         case ELFOSABI_LINUX:
            printf("Linux\n");
            break;
        
         case ELFOSABI_SOLARIS:
            printf("Solaris\n");
            break;
        
         case ELFOSABI_IRIX:
            printf("IRIX\n");
            break;
         
         case ELFOSABI_FREEBSD:
            printf("FreeBSD\n");
            break;
        
         case ELFOSABI_TRU64:
            printf("TRU64 UNIX\n");
            break;
        
         case ELFOSABI_ARM:
            printf("ARM architecture\n");
            break;
        
         case ELFOSABI_STANDALONE:
            printf("Stand-alone (embedded)\n");
            break;
      }

      // ABI Version
      printf("  ABI Version:                       %d\n", header->e_ident[8]); 

      // Type
      printf("  Type:                              ");
      switch(ntohs(header->e_type)) {
         case ET_REL:
            printf("REL (Relocatable file)\n");
            break;
         
         case ET_EXEC:
            printf("EXEC (Executable file)\n");
            break;
         
         case ET_DYN:
            printf("DYN (Shared object file)\n");
            break;

         case ET_CORE:
            printf("CORE (Core file)\n");
            break;

      }

      // Machine
      printf("  Machine:                           ");
      switch(ntohs(header->e_machine)) {
         case EM_M32:
            printf("AT&T WE 32100\n");
            break;

         case EM_SPARC:
            printf("Sun Microsystems SPARC\n");
            break;
         
         case EM_386:
            printf("Intel 80386\n");
            break;

         case EM_68K:
            printf("Motorola 68000\n");
            break;
         
         case EM_88K:
            printf("Motorola 88000\n");
            break;

         case EM_860:
            printf("Intel 80860\n");
            break;

         case EM_MIPS:
            printf("MIPS RS3000 (big-endian only\n");
            break;

         case EM_PARISC:
            printf("HP/PA\n");
            break;
         
         case EM_SPARC32PLUS:
            printf("SPARC with enhanced instruction set\n");
            break;

         case EM_PPC:
            printf("PowerPC\n");
            break;
         
         case EM_PPC64:
            printf("PowerPC64\n");
            break;
         
         case EM_S390:
            printf("IBM S/390\n");
            break;
         
         case EM_ARM:
            printf("Advanced RISC Machines\n");
            break;
         
         case EM_SH:
            printf("Renesas SuperH\n");
            break;
         
         case EM_SPARCV9:
            printf("SPARC V9 64-bit\n");
            break;

         case EM_IA_64:
            printf("Intel Itanium\n");
            break;
         
         case EM_X86_64:
            printf("Advanced Micro Devices X86-64\n");
            break;
         
         case EM_VAX:
            printf("DEC Vax\n");
            break;

			case EM_AARCH64:
            printf("AArch64\n");
            break;
      }

      // Version
      if (ntohl(header->e_version) == EV_CURRENT) {
         printf("  Version:                           0x1\n");
      }

      // Entry point address
      printf("  Entry point address:               %p\n", (void *) header->e_entry);

      // Start of program headers
      printf("  Start of program headers:          %ld (bytes into file)\n", header->e_phoff);
     
      // Start of section headers
      printf("  Start of section headers:          %ld (bytes into file)\n", header->e_shoff);
     
      // Flags
      printf("  Flags:                             0x0\n");

      // Size of this header
      printf("  Size of this header:               %d (bytes)\n", header->e_ehsize);

      // Size of program headers
      printf("  Size of program headers:           %d (bytes)\n", header->e_phentsize);
      
      // Number of program headers
      printf("  Number of program headers:         %d\n", header->e_phnum);

      // Size of section headers
      printf("  Size of section headers:           %d (bytes)\n", header->e_shentsize);

      // Number of section headers
      printf("  Number of section headers:         %d\n", header->e_shnum);
      
      // Section header string table index
      printf("  Section header string table index: %d\n", header->e_shstrndx);

      }

      return 0;

}


/*
 * @brief prints the section information
 * @param Elf32_Ehdr header struct for ELF header
 * @param void * file_pointer pointer ot the beginning of the mmapped ELF file
 * @param void * string_table_pointer pointer to the beginning of the section header string table
 */
void print_sections_32(Elf32_Ehdr header, void * file_pointer, void * string_table_pointer) {

   // point to the beginning of the section headers
   Elf32_Shdr * section_header = file_pointer + header.e_shoff;

   // If the ELF and machine share the same endianness, no need to swap endianness
   if(isSameEndianness()) {

      printf("There are %d section headers, starting at offset 0x%x:\n\n", header.e_shnum, header.e_shoff);
   
      printf("Section Headers:\n");
   
      printf("  [Nr] %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s\n", "Name", "Type", "Address", "Offset", "Link", "Size", "EntSize", "Info", "Align", "Flags");
   
      for (int i=0; i<header.e_shnum; i++) {
         printf("  [%2d]", i);
        
         // Check if there's a name
         if (section_header->sh_name != 0) {
           printf(" %s\n", (char *)string_table_pointer + section_header->sh_name);
         } else { // if not, just print a new line
            printf("\n");
         }
   
         // Print type
   	   switch(section_header->sh_type) {
   	      case SHT_NULL:
   	         printf("       %-16s ", "NULL");
   	         break;
   	      case SHT_PROGBITS:
   	         printf("       %-16s ", "PROGBITS");
   	         break;
   	      case SHT_SYMTAB:
   	         printf("       %-16s ", "SYMTAB");
   	         break;
   	      case SHT_STRTAB:
   	         printf("       %-16s ", "STRTAB");
   	         break;
   	      case SHT_RELA:
   	         printf("       %-16s ", "RELA");
   	         break;
   	      case SHT_HASH:
   	         printf("       %-16s ", "HASH");
   	         break;
   	      case SHT_DYNAMIC:
   	         printf("       %-16s ", "DYNAMIC");
   	         break;
   	      case SHT_NOTE:
   	         printf("       %-16s ", "NOTE");
   	         break;
   	      case SHT_NOBITS:
   	         printf("       %-16s ", "NOBITS");
   	         break;
   	      case SHT_REL:
   	         printf("       %-16s ", "REL");
   	         break;
   	      case SHT_SHLIB:
   	         printf("       %-16s ", "SHLIB");
   	         break;
   	      case SHT_DYNSYM:
   	         printf("       %-16s ", "DYNSYM");
   	         break;
   	      case SHT_INIT_ARRAY:
   	         printf("       %-16s ", "INIT_ARRAY");
   	         break;
   	      case SHT_FINI_ARRAY:
   	         printf("       %-16s ", "FINI_ARRAY");
   	         break;
   	      case SHT_PREINIT_ARRAY:
   	         printf("       %-16s ", "PREINIT_ARRAY");
   	         break;
   	      case SHT_GROUP:
   	         printf("       %-16s ", "GROUP");
   	         break;
   	      case SHT_GNU_ATTRIBUTES:
   	         printf("       %-16s ", "GNU_ATTRIBUTES");
   	         break;
   	      case SHT_GNU_HASH:
   	         printf("       %-16s ", "GNU_HASH");
   	         break;
   	      case SHT_GNU_LIBLIST:
   	         printf("       %-16s ", "GNU_LIBLIST");
   	         break;
   	      case SHT_CHECKSUM:
   	         printf("       %-16s ", "CHECKSUM");
   	         break;
   	      case SHT_SUNW_move:
   	         printf("       %-16s ", "SUNW_MOVE");
   	         break;
   	      case SHT_SUNW_COMDAT:
   	         printf("       %-16s ", "SUNW_COMDAT");
   	         break;
   	      case SHT_SUNW_syminfo:
   	         printf("       %-16s ", "SUNW_SYMINFO");
   	         break;
   	      case SHT_GNU_verdef:
   	         printf("       %-16s ", "VERDEF");
   	         break;
   	      case SHT_GNU_verneed:
   	         printf("       %-16s ", "VERNEED");
   	         break;
   	      case SHT_GNU_versym:
   	         printf("       %-16s ", "VERSYM");
   	         break;
   	   }
   
         // print address
         printf("%016x  ", section_header->sh_addr);
   
         // print offset
         printf("%016x ", section_header->sh_offset);
   
         // print link
         printf("%-16d\n", section_header->sh_link);
   
         // print size
         printf("       %016x ", section_header->sh_size);
         
         // print entsize
         printf("%016x  ", section_header->sh_entsize);
   
         // print info
         printf("%-16d ", section_header->sh_info);
         
         // print align
         printf("%-16d\n", section_header->sh_addralign);
   
         // print flags
         printf("       [%016x]:\n", section_header->sh_flags);
   
         section_header++;
      }

   } else { // Endian mismatch

      printf("There are %d section headers, starting at offset 0x%x:\n\n", swapUint16(header.e_shnum), swapUint32(header.e_shoff));
   
      printf("Section Headers:\n");
   
      printf("  [Nr] %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s\n", "Name", "Type", "Address", "Offset", "Link", "Size", "EntSize", "Info", "Align", "Flags");
   
      for (int i=0; i<swapUint16(header.e_shnum); i++) {
         printf("  [%2d]", i);
        
         // Check if there's a name
         if (swapUint32(section_header->sh_name) != 0) {
           printf(" %s\n", (char *)string_table_pointer + swapUint32(section_header->sh_name));
         } else { // if not, just print a new line
            printf("\n");
         }
   
         // Print type
   	   switch(swapUint32(section_header->sh_type)) {
   	      case SHT_NULL:
   	         printf("       %-16s ", "NULL");
   	         break;
   	      case SHT_PROGBITS:
   	         printf("       %-16s ", "PROGBITS");
   	         break;
   	      case SHT_SYMTAB:
   	         printf("       %-16s ", "SYMTAB");
   	         break;
   	      case SHT_STRTAB:
   	         printf("       %-16s ", "STRTAB");
   	         break;
   	      case SHT_RELA:
   	         printf("       %-16s ", "RELA");
   	         break;
   	      case SHT_HASH:
   	         printf("       %-16s ", "HASH");
   	         break;
   	      case SHT_DYNAMIC:
   	         printf("       %-16s ", "DYNAMIC");
   	         break;
   	      case SHT_NOTE:
   	         printf("       %-16s ", "NOTE");
   	         break;
   	      case SHT_NOBITS:
   	         printf("       %-16s ", "NOBITS");
   	         break;
   	      case SHT_REL:
   	         printf("       %-16s ", "REL");
   	         break;
   	      case SHT_SHLIB:
   	         printf("       %-16s ", "SHLIB");
   	         break;
   	      case SHT_DYNSYM:
   	         printf("       %-16s ", "DYNSYM");
   	         break;
   	      case SHT_INIT_ARRAY:
   	         printf("       %-16s ", "INIT_ARRAY");
   	         break;
   	      case SHT_FINI_ARRAY:
   	         printf("       %-16s ", "FINI_ARRAY");
   	         break;
   	      case SHT_PREINIT_ARRAY:
   	         printf("       %-16s ", "PREINIT_ARRAY");
   	         break;
   	      case SHT_GROUP:
   	         printf("       %-16s ", "GROUP");
   	         break;
   	      case SHT_GNU_ATTRIBUTES:
   	         printf("       %-16s ", "GNU_ATTRIBUTES");
   	         break;
   	      case SHT_GNU_HASH:
   	         printf("       %-16s ", "GNU_HASH");
   	         break;
   	      case SHT_GNU_LIBLIST:
   	         printf("       %-16s ", "GNU_LIBLIST");
   	         break;
   	      case SHT_CHECKSUM:
   	         printf("       %-16s ", "CHECKSUM");
   	         break;
   	      case SHT_SUNW_move:
   	         printf("       %-16s ", "SUNW_MOVE");
   	         break;
   	      case SHT_SUNW_COMDAT:
   	         printf("       %-16s ", "SUNW_COMDAT");
   	         break;
   	      case SHT_SUNW_syminfo:
   	         printf("       %-16s ", "SUNW_SYMINFO");
   	         break;
   	      case SHT_GNU_verdef:
   	         printf("       %-16s ", "VERDEF");
   	         break;
   	      case SHT_GNU_verneed:
   	         printf("       %-16s ", "VERNEED");
   	         break;
   	      case SHT_GNU_versym:
   	         printf("       %-16s ", "VERSYM");
   	         break;
   	   }
   
         // print address
         printf("%016x  ", swapUint32(section_header->sh_addr) );
   
         // print offset
         printf("%016x ", swapUint32(section_header->sh_offset) );
   
         // print link
         printf("%-16d\n", swapUint32(section_header->sh_link) );
   
         // print size
         printf("       %016x ", swapUint32(section_header->sh_size) );
         
         // print entsize
         printf("%016x  ", swapUint32(section_header->sh_entsize) );
   
         // print info
         printf("%-16d ", swapUint32(section_header->sh_info) );
         
         // print align
         printf("%-16d\n", swapUint32(section_header->sh_addralign) );
   
         // print flags
         printf("       [%016x]:\n", swapUint32(section_header->sh_flags) );
   
         section_header++;
      }

   }
}


/*
 * @brief prints the section information
 * @param Elf64_Ehdr header struct for ELF header
 * @param void * file_pointer pointer ot the beginning of the mmapped ELF file
 * @param void * string_table_pointer pointer to the beginning of the section header string table
 */
void print_sections_64(Elf64_Ehdr header, void * file_pointer, void * string_table_pointer) {

   // point to the beginning of the section headers
   Elf64_Shdr * section_header = file_pointer + header.e_shoff;

   // If the ELF and machine share the same endianness, no need to swap endianness
   if(isSameEndianness()) {

      printf("There are %d section headers, starting at offset 0x%lx:\n\n", header.e_shnum, header.e_shoff);
   
      printf("Section Headers:\n");
   
      printf("  [Nr] %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s\n", "Name", "Type", "Address", "Offset", "Link", "Size", "EntSize", "Info", "Align", "Flags");
   
      for (int i=0; i<header.e_shnum; i++) {
         printf("  [%2d]", i);
        
         // Check if there's a name
         if (section_header->sh_name != 0) {
           printf(" %s\n", (char *)string_table_pointer + section_header->sh_name);
         } else { // if not, just print a new line
            printf("\n");
         }
   
         // Print type
   	   switch(section_header->sh_type) {
   	      case SHT_NULL:
   	         printf("       %-16s ", "NULL");
   	         break;
   	      case SHT_PROGBITS:
   	         printf("       %-16s ", "PROGBITS");
   	         break;
   	      case SHT_SYMTAB:
   	         printf("       %-16s ", "SYMTAB");
   	         break;
   	      case SHT_STRTAB:
   	         printf("       %-16s ", "STRTAB");
   	         break;
   	      case SHT_RELA:
   	         printf("       %-16s ", "RELA");
   	         break;
   	      case SHT_HASH:
   	         printf("       %-16s ", "HASH");
   	         break;
   	      case SHT_DYNAMIC:
   	         printf("       %-16s ", "DYNAMIC");
   	         break;
   	      case SHT_NOTE:
   	         printf("       %-16s ", "NOTE");
   	         break;
   	      case SHT_NOBITS:
   	         printf("       %-16s ", "NOBITS");
   	         break;
   	      case SHT_REL:
   	         printf("       %-16s ", "REL");
   	         break;
   	      case SHT_SHLIB:
   	         printf("       %-16s ", "SHLIB");
   	         break;
   	      case SHT_DYNSYM:
   	         printf("       %-16s ", "DYNSYM");
   	         break;
   	      case SHT_INIT_ARRAY:
   	         printf("       %-16s ", "INIT_ARRAY");
   	         break;
   	      case SHT_FINI_ARRAY:
   	         printf("       %-16s ", "FINI_ARRAY");
   	         break;
   	      case SHT_PREINIT_ARRAY:
   	         printf("       %-16s ", "PREINIT_ARRAY");
   	         break;
   	      case SHT_GROUP:
   	         printf("       %-16s ", "GROUP");
   	         break;
   	      case SHT_GNU_ATTRIBUTES:
   	         printf("       %-16s ", "GNU_ATTRIBUTES");
   	         break;
   	      case SHT_GNU_HASH:
   	         printf("       %-16s ", "GNU_HASH");
   	         break;
   	      case SHT_GNU_LIBLIST:
   	         printf("       %-16s ", "GNU_LIBLIST");
   	         break;
   	      case SHT_CHECKSUM:
   	         printf("       %-16s ", "CHECKSUM");
   	         break;
   	      case SHT_SUNW_move:
   	         printf("       %-16s ", "SUNW_MOVE");
   	         break;
   	      case SHT_SUNW_COMDAT:
   	         printf("       %-16s ", "SUNW_COMDAT");
   	         break;
   	      case SHT_SUNW_syminfo:
   	         printf("       %-16s ", "SUNW_SYMINFO");
   	         break;
   	      case SHT_GNU_verdef:
   	         printf("       %-16s ", "VERDEF");
   	         break;
   	      case SHT_GNU_verneed:
   	         printf("       %-16s ", "VERNEED");
   	         break;
   	      case SHT_GNU_versym:
   	         printf("       %-16s ", "VERSYM");
   	         break;
   	   }
   
         // print address
         printf("%016lx  ", section_header->sh_addr);
   
         // print offset
         printf("%016lx ", section_header->sh_offset);
   
         // print link
         printf("%-16d\n", section_header->sh_link);
   
         // print size
         printf("       %016lx ", section_header->sh_size);
         
         // print entsize
         printf("%016lx  ", section_header->sh_entsize);
   
         // print info
         printf("%-16d ", section_header->sh_info);
         
         // print align
         printf("%-16ld\n", section_header->sh_addralign);
   
         // print flags
         printf("       [%016lx]:\n", section_header->sh_flags);
   
         section_header++;
      }

   } else { // Endian mismatch

      printf("There are %d section headers, starting at offset 0x%lx:\n\n", swapUint16(header.e_shnum), swapUint64(header.e_shoff));
   
      printf("Section Headers:\n");
   
      printf("  [Nr] %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s %-16s  %-16s %-16s\n       %-16s\n", "Name", "Type", "Address", "Offset", "Link", "Size", "EntSize", "Info", "Align", "Flags");
   
      for (int i=0; i<swapUint16(header.e_shnum); i++) {
         printf("  [%2d]", i);
        
         // Check if there's a name
         if (swapUint32(section_header->sh_name) != 0) {
           printf(" %s\n", (char *)string_table_pointer + swapUint32(section_header->sh_name));
         } else { // if not, just print a new line
            printf("\n");
         }
   
         // Print type
   	   switch(swapUint32(section_header->sh_type)) {
   	      case SHT_NULL:
   	         printf("       %-16s ", "NULL");
   	         break;
   	      case SHT_PROGBITS:
   	         printf("       %-16s ", "PROGBITS");
   	         break;
   	      case SHT_SYMTAB:
   	         printf("       %-16s ", "SYMTAB");
   	         break;
   	      case SHT_STRTAB:
   	         printf("       %-16s ", "STRTAB");
   	         break;
   	      case SHT_RELA:
   	         printf("       %-16s ", "RELA");
   	         break;
   	      case SHT_HASH:
   	         printf("       %-16s ", "HASH");
   	         break;
   	      case SHT_DYNAMIC:
   	         printf("       %-16s ", "DYNAMIC");
   	         break;
   	      case SHT_NOTE:
   	         printf("       %-16s ", "NOTE");
   	         break;
   	      case SHT_NOBITS:
   	         printf("       %-16s ", "NOBITS");
   	         break;
   	      case SHT_REL:
   	         printf("       %-16s ", "REL");
   	         break;
   	      case SHT_SHLIB:
   	         printf("       %-16s ", "SHLIB");
   	         break;
   	      case SHT_DYNSYM:
   	         printf("       %-16s ", "DYNSYM");
   	         break;
   	      case SHT_INIT_ARRAY:
   	         printf("       %-16s ", "INIT_ARRAY");
   	         break;
   	      case SHT_FINI_ARRAY:
   	         printf("       %-16s ", "FINI_ARRAY");
   	         break;
   	      case SHT_PREINIT_ARRAY:
   	         printf("       %-16s ", "PREINIT_ARRAY");
   	         break;
   	      case SHT_GROUP:
   	         printf("       %-16s ", "GROUP");
   	         break;
   	      case SHT_GNU_ATTRIBUTES:
   	         printf("       %-16s ", "GNU_ATTRIBUTES");
   	         break;
   	      case SHT_GNU_HASH:
   	         printf("       %-16s ", "GNU_HASH");
   	         break;
   	      case SHT_GNU_LIBLIST:
   	         printf("       %-16s ", "GNU_LIBLIST");
   	         break;
   	      case SHT_CHECKSUM:
   	         printf("       %-16s ", "CHECKSUM");
   	         break;
   	      case SHT_SUNW_move:
   	         printf("       %-16s ", "SUNW_MOVE");
   	         break;
   	      case SHT_SUNW_COMDAT:
   	         printf("       %-16s ", "SUNW_COMDAT");
   	         break;
   	      case SHT_SUNW_syminfo:
   	         printf("       %-16s ", "SUNW_SYMINFO");
   	         break;
   	      case SHT_GNU_verdef:
   	         printf("       %-16s ", "VERDEF");
   	         break;
   	      case SHT_GNU_verneed:
   	         printf("       %-16s ", "VERNEED");
   	         break;
   	      case SHT_GNU_versym:
   	         printf("       %-16s ", "VERSYM");
   	         break;
   	   }
   
         // print address
         printf("%016lx  ", swapUint64(section_header->sh_addr) );
   
         // print offset
         printf("%016lx ", swapUint64(section_header->sh_offset) );
   
         // print link
         printf("%-16d\n", swapUint32(section_header->sh_link) );
   
         // print size
         printf("       %016lx ", swapUint64(section_header->sh_size) );
         
         // print entsize
         printf("%016lx  ", swapUint64(section_header->sh_entsize) );
   
         // print info
         printf("%-16d ", swapUint32(section_header->sh_info) );
         
         // print align
         printf("%-16ld\n", swapUint64(section_header->sh_addralign) );
   
         // print flags
         printf("       [%016lx]:\n", swapUint64(section_header->sh_flags) );
   
         section_header++;
      }

   }

}

void hex_dump_32(Elf32_Ehdr header, void * file_pointer, void * string_table_pointer, char * section) {

   int section_index; // index of section to be printed
   void * start;
   bool match = false; // true if a match is found for section either through name or index
   Elf32_Shdr * section_header = file_pointer + header.e_shoff;

   if(isSameEndianness()) {
      // if trying to print by section name
      if (section[0] == '.') {

         // Find the secton index
         for (int i=0; i<header.e_shnum; i++) {
          
            if (strncmp((char *)string_table_pointer + section_header->sh_name, section, 20) == 0) {
               section_index = i;
               match = true;
            }
            section_header++;
         }

         // If the section was not found
         if (match == false) {
            fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
            exit(EXIT_FAILURE);
         }

         // reset section_header pointer to beginning
         section_header = file_pointer + header.e_shoff;
      } else if (isNum(section)){ // Trying to find by section number
         section_index = atoi(section);

         // If seciton_index is not a valid section index
         if ( (section_index > header.e_shnum) || (section_index < 0) ) {
            fprintf(stderr, "readelf: Warning: Section %d was not dumped because it does not exist!\n", section_index);
            exit(EXIT_FAILURE);
         }
      } else {
         fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
         exit(EXIT_FAILURE);
      }

   // At this point section_index should have a correct value

      match = false;
      for (int i=0; i<header.e_shnum; i++) {
         if ( section_index == i ) {
            match = true;
            break;
         }
         section_header++;
      }
     
      // At this point start and end should contain the start and end addresses
      if (match == true) {

         start = section_header->sh_offset + file_pointer;
        
         printf("\nHex dump of section '%s':", (char *)string_table_pointer + section_header->sh_name);

         int i; // This needs to be declared outisde the for loop for scope
         for (i=0; i<section_header->sh_size; i++) {
            if ( (i % 16) == 0 ) {

               // Don't print on the first line
               if ( i > 0 ) {
                  printf(" ");
                  
                  // Print the ASCII representation
                  for (int j=i-16; j<i; j++) {
                     if(isAscii(*((char *)start + j))) {
                        printf("%c", *((char *)start + j));
                     } else {
                       printf(".");
                     }
                  }
               }
               printf("\n  0x%08x ", i);
            } else if( (i%4) == 0 ) {
               printf(" ");
            }
            printf("%02x", *((char *)start + i)); 

         }

         // pad for the rest of the hex dump
         for(int k=0; k < (16 - (i%16)); k++) {
            if ( (k%4) == 0 ) {
               printf(" ");
            }
            printf("  ");
         }
         
         // Print the last line of ASCII representation
         for(int k=i-(i%16); k < i; k++) {
            if(isAscii(*((char *)start + k))) {
               printf("%c", *((char *)start + k));
            } else {
               printf(".");
            }
         }

         printf("\n\n");

      } else {
         exit(EXIT_FAILURE);
      }

   } else { // Endianness mismatch

      // if trying to print by section name
      if (section[0] == '.') {

         // Find the secton index
         for (int i=0; i<swapUint16(header.e_shnum); i++) {
          
            if (strncmp((char *)string_table_pointer + swapUint64(section_header->sh_name), section, 20) == 0) {
               section_index = i;
               match = true;
            }
            section_header++;
         }

         // If the section was not found
         if (match == false) {
            fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
            exit(EXIT_FAILURE);
         }

         // reset section_header pointer to beginning
         section_header = file_pointer + swapUint64(header.e_shoff);
      } else if (isNum(section)){ // Trying to find by section number
         section_index = atoi(section);

         // If seciton_index is not a valid section index
         if ( (section_index > swapUint16(header.e_shnum)) || (section_index < 0) ) {
            fprintf(stderr, "readelf: Warning: Section %d was not dumped because it does not exist!\n", section_index);
            exit(EXIT_FAILURE);
         }
      } else {
         fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
         exit(EXIT_FAILURE);
      }

   // At this point section_index should have a correct value

      match = false;
      for (int i=0; i<swapUint16(header.e_shnum); i++) {
         if ( section_index == i ) {
            match = true;
            break;
         }
         section_header++;
      }
     
      // At this point start and end should contain the start and end addresses
      if (match == true) {

         start = swapUint64(section_header->sh_offset) + file_pointer;
        
         printf("\nHex dump of section '%s':", (char *)string_table_pointer + swapUint32(section_header->sh_name));

         int i; // This needs to be declared outisde the for loop for scope
         for (i=0; i<swapUint16(section_header->sh_size); i++) {
            if ( (i % 16) == 0 ) {

               // Don't print on the first line
               if ( i > 0 ) {
                  printf(" ");
                  
                  // Print the ASCII representation
                  for (int j=i-16; j<i; j++) {
                     if(isAscii(*((char *)start + j))) {
                        printf("%c", *((char *)start + j));
                     } else {
                       printf(".");
                     }
                  }
               }
               printf("\n  0x%08x ", i);
            } else if( (i%4) == 0 ) {
               printf(" ");
            }
            printf("%02x", *((char *)start + i)); 

         }

         // pad for the rest of the hex dump
         for(int k=0; k < (16 - (i%16)); k++) {
            if ( (k%4) == 0 ) {
               printf(" ");
            }
            printf("  ");
         }
         
         // Print the last line of ASCII representation
         for(int k=i-(i%16); k < i; k++) {
            if(isAscii(*((char *)start + k))) {
               printf("%c", *((char *)start + k));
            } else {
               printf(".");
            }
         }

         printf("\n\n");

      } else {
         exit(EXIT_FAILURE);
      }
   }

}

void hex_dump_64(Elf64_Ehdr header, void * file_pointer, void * string_table_pointer, char * section) {

   int section_index; // index of section to be printed
   void * start;
   bool match = false; // true if a match is found for section either through name or index
   Elf64_Shdr * section_header = file_pointer + header.e_shoff;

   if(isSameEndianness()) {
      // if trying to print by section name
      if (section[0] == '.') {

         // Find the secton index
         for (int i=0; i<header.e_shnum; i++) {
          
            if (strncmp((char *)string_table_pointer + section_header->sh_name, section, 20) == 0) {
               section_index = i;
               match = true;
            }
            section_header++;
         }

         // If the section was not found
         if (match == false) {
            fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
            exit(EXIT_FAILURE);
         }

         // reset section_header pointer to beginning
         section_header = file_pointer + header.e_shoff;
      } else if (isNum(section)){ // Trying to find by section number
         section_index = atoi(section);

         // If seciton_index is not a valid section index
         if ( (section_index > header.e_shnum) || (section_index < 0) ) {
            fprintf(stderr, "readelf: Warning: Section %d was not dumped because it does not exist!\n", section_index);
            exit(EXIT_FAILURE);
         }
      } else {
         fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
         exit(EXIT_FAILURE);
      }

   // At this point section_index should have a correct value

      match = false;
      for (int i=0; i<header.e_shnum; i++) {
         if ( section_index == i ) {
            match = true;
            break;
         }
         section_header++;
      }
     
      // At this point start and end should contain the start and end addresses
      if (match == true) {

         start = section_header->sh_offset + file_pointer;
        
         printf("\nHex dump of section '%s':", (char *)string_table_pointer + section_header->sh_name);

         int i; // This needs to be declared outisde the for loop for scope
         for (i=0; i<section_header->sh_size; i++) {
            if ( (i % 16) == 0 ) {

               // Don't print on the first line
               if ( i > 0 ) {
                  printf(" ");
                  
                  // Print the ASCII representation
                  for (int j=i-16; j<i; j++) {
                     if(isAscii(*((char *)start + j))) {
                        printf("%c", *((char *)start + j));
                     } else {
                       printf(".");
                     }
                  }
               }
               printf("\n  0x%08x ", i);
            } else if( (i%4) == 0 ) {
               printf(" ");
            }
            printf("%02x", *((char *)start + i)); 

         }

         // pad for the rest of the hex dump
         for(int k=0; k < (16 - (i%16)); k++) {
            if ( (k%4) == 0 ) {
               printf(" ");
            }
            printf("  ");
         }
         
         // Print the last line of ASCII representation
         for(int k=i-(i%16); k < i; k++) {
            if(isAscii(*((char *)start + k))) {
               printf("%c", *((char *)start + k));
            } else {
               printf(".");
            }
         }

         printf("\n\n");

      } else {
         exit(EXIT_FAILURE);
      }

   } else { // Endianness mismatch

      // if trying to print by section name
      if (section[0] == '.') {

         // Find the secton index
         for (int i=0; i<swapUint16(header.e_shnum); i++) {
          
            if (strncmp((char *)string_table_pointer + swapUint64(section_header->sh_name), section, 20) == 0) {
               section_index = i;
               match = true;
            }
            section_header++;
         }

         // If the section was not found
         if (match == false) {
            fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
            exit(EXIT_FAILURE);
         }

         // reset section_header pointer to beginning
         section_header = file_pointer + swapUint64(header.e_shoff);
      } else if (isNum(section)){ // Trying to find by section number
         section_index = atoi(section);

         // If seciton_index is not a valid section index
         if ( (section_index > swapUint16(header.e_shnum)) || (section_index < 0) ) {
            fprintf(stderr, "readelf: Warning: Section %d was not dumped because it does not exist!\n", section_index);
            exit(EXIT_FAILURE);
         }
      } else {
         fprintf(stderr, "readelf: Warning: Section '%s' was not dumped because it does not exist!\n", section);
         exit(EXIT_FAILURE);
      }

   // At this point section_index should have a correct value

      match = false;
      for (int i=0; i<swapUint16(header.e_shnum); i++) {
         if ( section_index == i ) {
            match = true;
            break;
         }
         section_header++;
      }
     
      // At this point start and end should contain the start and end addresses
      if (match == true) {

         start = swapUint64(section_header->sh_offset) + file_pointer;
        
         printf("\nHex dump of section '%s':", (char *)string_table_pointer + swapUint32(section_header->sh_name));

         int i; // This needs to be declared outisde the for loop for scope
         for (i=0; i<swapUint16(section_header->sh_size); i++) {
            if ( (i % 16) == 0 ) {

               // Don't print on the first line
               if ( i > 0 ) {
                  printf(" ");
                  
                  // Print the ASCII representation
                  for (int j=i-16; j<i; j++) {
                     if(isAscii(*((char *)start + j))) {
                        printf("%c", *((char *)start + j));
                     } else {
                       printf(".");
                     }
                  }
               }
               printf("\n  0x%08x ", i);
            } else if( (i%4) == 0 ) {
               printf(" ");
            }
            printf("%02x", *((char *)start + i)); 

         }

         // pad for the rest of the hex dump
         for(int k=0; k < (16 - (i%16)); k++) {
            if ( (k%4) == 0 ) {
               printf(" ");
            }
            printf("  ");
         }
         
         // Print the last line of ASCII representation
         for(int k=i-(i%16); k < i; k++) {
            if(isAscii(*((char *)start + k))) {
               printf("%c", *((char *)start + k));
            } else {
               printf(".");
            }
         }

         printf("\n\n");

      } else {
         exit(EXIT_FAILURE);
      }
   }
}



