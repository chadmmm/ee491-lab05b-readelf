#ifndef PROCESS_SECTIONS_H
#define PROCESS_SECTIONS_H
///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05b - Read ELF
/////
///// @file process_sections.h
///// @version 1.0
/////
///// @author Chad Morita <chadmmm@hawaii.edu>
///// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
///// @date   21_02_2021
/////////////////////////////////////////////////////////////////////////////////
#include "elf.h"

void process_sections_32(char *, Elf32_Ehdr, void **, size_t, void **);
void process_sections_64(char *, Elf64_Ehdr, void **, size_t, void **);

#endif
