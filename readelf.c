///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Read ELF
///
/// @file readelf.c
/// @version 1.0
///
/// @author Chad Morita <chadmmm@hawaii.edu>
/// @brief  Lab 05b - Read ELF - EE 491F - Spr 2021
/// @date   21_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include "readelf.h"
#include "process_header.h"
#include "elf.h"
#include "printer.h"
#include "process_sections.h"

// Used to keep track of program state global variable
ProgramState program_state;

/*
 * @brief Main function for the readelf program. Processes options
 * @return int 0 success, 1 failure
 *
 */
int main(int argc, char * argv[]) {

   int option; // holds return value from getopt
   Elf32_Ehdr header32;
   Elf64_Ehdr header64;
   FILE * fp = NULL;
   void * file_pointer = NULL;
   void * string_table_pointer = NULL;
   size_t file_size = 0;
   

   // Process input options

   // Continue reading options until there are no others
   while ( (option = getopt(argc, argv, "h:t:x:")) != -1) {

      switch(option) {
         // -h option
         case 'h':
            initialize_state(optarg);

            if( program_state.elf_architecture == ELFCLASS32 ) {
               process_header_32(optarg, &header32, &fp);
               print_header_32(&header32);
               fclose(fp);
            } else if( program_state.elf_architecture == ELFCLASS64 ){
               process_header_64(optarg, &header64, &fp);
               print_header_64(&header64);
               fclose(fp);
            }
            break;
         case 't':
            initialize_state(optarg);
            if( program_state.elf_architecture == ELFCLASS32 ) {
               process_header_32(optarg, &header32, &fp);
               process_sections_32(optarg, header32, &file_pointer, file_size, &string_table_pointer);
               print_sections_32(header32, file_pointer, string_table_pointer);
               fclose(fp);
               munmap(file_pointer, file_size);
            } else if( program_state.elf_architecture == ELFCLASS64 ){
               process_header_64(optarg, &header64, &fp);
               process_sections_64(optarg, header64, &file_pointer, file_size, &string_table_pointer);
               print_sections_64(header64, file_pointer, string_table_pointer);
               fclose(fp);
               munmap(file_pointer, file_size);
            }
            break;
         case 'x':
            initialize_state(argv[optind]);
            if( program_state.elf_architecture == ELFCLASS32 ) {
               process_header_32(argv[optind], &header32, &fp);
               process_sections_32(argv[optind], header32, &file_pointer, file_size, &string_table_pointer);
               hex_dump_32(header32, file_pointer, string_table_pointer, optarg);
               fclose(fp);
               munmap(file_pointer, file_size);
            } else if ( program_state.elf_architecture == ELFCLASS64 ) {
               process_header_64(argv[optind], &header64, &fp);
               process_sections_64(argv[optind], header64, &file_pointer, file_size,  &string_table_pointer);
               hex_dump_64(header64, file_pointer, string_table_pointer, optarg);
               fclose(fp);
               munmap(file_pointer, file_size);
            }
            break;
         // unknown option
         case '?':
         default:
            usage(); // print usage message
      }
   }
   
   // Adjust argc and argv to act normally
   argc -= optind;
   argv += optind;

   return 0;
}

/*
 * @brief Print useage information
 * @return void
 *
 * */
void usage() {
   printf("Usage: readelf <option(s)> elf-file(s)\n");
   printf(" Display information about the contents of ELF format files\n");
   printf(" Options are:\n");
   printf("  -h                     Display the ELF file header\n");
   printf("  -t                     Display the section details\n");
}


